﻿var itemsPerPage = 10,
    $selectGenre = $("#select-genre"),
    $selectGenreModal = $("#select-genre-modal");

// Initialize Genres
$.getJSON('../api/genres/', function (genres) {
    var gdata = [{ id: 0, text: ''}];
    $.each(genres, function (index, genre) {
        gdata.push({ id: genre.GenreId, text: genre.Name });
    });

    $selectGenre.select2({
        theme: "bootstrap",
        placeholder: {
            id: 0,
            text: 'No selection'
        },
        allowClear: true,
        data: gdata
    }).on("select2:close", function (e) {
        this.focus();
    });

    $selectGenreModal.select2({
        theme: "bootstrap",
        dropdownParent: $("#myModal"),
        placeholder: {
            id: 0,
            text: 'No selection'
        },
        allowClear: true,
        data: gdata
    }).on("select2:close", function (e) {
        this.focus();
    });
})

var $selectTrackAjax = $("#select-track-ajax").select2({
    theme: "bootstrap",
    placeholder: {
        id: 0,
        text: 'No selection'
    },
    allowClear: true,
    ajax: {
        url: "../api/tracksearch",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            var query = {
                q: params.term, // search term
                g: $("#select-genre :selected").val(),
                pg: params.page
            };
            // Query parameters will be ?q=[term]&page=[page]
            return query;
        },
        processResults: function (data, params) {
            // parse the results into the format expected by Select2
            // since we are using custom formatting functions we do not need to
            // alter the remote JSON data, except to indicate that infinite
            // scrolling can be used
            data.totalCount = data.totalCount || itemsPerPage;
            params.page = params.page || 1;

            var newData = [{ id: 0, text: ''}];

            if (data.tracks) {
                $.each(data.tracks, function (index, d) {
                    newData.push({ id: d.TrackId, text: d.Name + ' [' + d.Genre + ']' });
                });
            }

            return {
                results: newData,
                pagination: {
                    more: (params.page * itemsPerPage) < data.totalCount
                }
            };
        },
        cache: true
    },
    escapeMarkup: function (markup) { return markup; },
    minimumInputLength: 1
    //    minimumInputLength: 1,
    //    templateResult: formatRepo,
    //    templateSelection: formatRepoSelection
}).on("select2:close", function (e) {
    this.focus();
});

var $selectTrackAjaxModal = $("#select-track-ajax-modal").select2({
    theme: "bootstrap",
    dropdownParent: $("#myModal"),
    placeholder: {
        id: 0,
        text: 'No selection'
    },
    allowClear: true,
    ajax: {
        url: "../api/tracksearch",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            var query = {
                q: params.term, // search term
                g: $("#select-genre-modal :selected").val(),
                pg: params.page
            };
            // Query parameters will be ?q=[term]&page=[page]
            return query;
        },
        processResults: function (data, params) {
            data.totalCount = data.totalCount || itemsPerPage;
            params.page = params.page || 1;

            var newData = [{ id: 0, text: ''}];

            if (data.tracks) {
                $.each(data.tracks, function (index, d) {
                    newData.push({ id: d.TrackId, text: d.Name + ' [' + d.Genre + ']' });
                });
            }

            return {
                results: newData,
                pagination: {
                    more: (params.page * itemsPerPage) < data.totalCount
                }
            };
        },
        cache: true
    },
    escapeMarkup: function (markup) { return markup; },
    minimumInputLength: 1
}).on("select2:close", function (e) {
    this.focus();
});

$('#myModal').on('shown.bs.modal', function () {
    setTimeout(function () {
        $selectGenreModal.focus();
    }, 1000);
})

$('#myModal-reset').on('click', function () {
    $selectGenreModal.val(null).trigger("change");
    $selectTrackAjaxModal.val(null).trigger("change");
    $selectGenreModal.focus();
});

$('#myReset').on('click', function () {
    $selectGenre.val(null).trigger("change");
    $selectTrackAjax.val(null).trigger("change");
    $selectGenre.focus();
});
