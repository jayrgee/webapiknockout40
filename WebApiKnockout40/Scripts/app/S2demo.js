﻿// Initialize Genres
$.getJSON('../api/genres/', function (genres) {
    var gdata = [{ id: 0, text: ''}];
    $.each(genres, function (index, genre) {
        gdata.push({ id: genre.GenreId, text: genre.Name });
    });

    $("#select-genre").select2({
        placeholder: {
            id: 0,
            text: 'No selection'
        },
        allowClear: true,
        data: gdata
    });
})

$("#select-track-ajax").select2({
    placeholder: {
        id: 0,
        text: 'No selection'
    },
    allowClear: true,
    ajax: {
        url: "../api/tracksearch",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            var query = {
                q: params.term, // search term
                g: $("#select-genre").val(),
                page: params.page
            };
            // Query parameters will be ?q=[term]&page=[page]
            return query;
        },
        processResults: function (data, params) {
            // parse the results into the format expected by Select2
            // since we are using custom formatting functions we do not need to
            // alter the remote JSON data, except to indicate that infinite
            // scrolling can be used
            params.page = params.page || 1;

            var newData = [{ id: 0, text: ''}];

            $.each(data, function (index, d) {
                newData.push({ id: d.TrackId, text: d.Name + ' [' + d.Genre + ']' });
            });

            return {
                results: newData,
                pagination: {
                    //more: (params.page * 30) < data.total_count
                    more: (params.page * 30) < newData.length
                }
            };
        },
        cache: true
    },
    escapeMarkup: function (markup) { return markup; },
    minimumInputLength: 1
    //    minimumInputLength: 1,
    //    templateResult: formatRepo,
    //    templateSelection: formatRepoSelection
});