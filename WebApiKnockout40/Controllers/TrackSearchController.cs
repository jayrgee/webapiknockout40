﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiKnockout40.Model;
using WebApiKnockout40.Model.DTO;

namespace WebApiKnockout40.Controllers
{
    public class TrackSearchController : ApiController
    {
        private int _defaultPageSize = 10;
        private int _minimumPageSize = 1;
        private int _defaultPageNo = 1;
        private int _minimumPageNo = 1;

        private int ParseIntWithDefault(string val, int dflt = 0, int min = 0)
        {
            int result;

            if (Int32.TryParse(val, out result))
            {
                return result < min ? dflt : result;
            }
            else
            {
                return dflt;
            }
        }

        struct PageAPIOptions
        {
            private int _pageNo;
            private int _pageSize;
            private int _skipCount;

            public PageAPIOptions(int pageNo, int pageSize, int skipCount)
            {
                _pageNo = pageNo;
                _pageSize = pageSize;
                _skipCount = skipCount;
            }

            public int PageNo { get { return _pageNo; }}
            public int PageSize { get { return _pageSize; } }
            public int SkipCount { get { return _skipCount; } }
        }

        private PageAPIOptions ParsePageAPIOptions(string pg, string pgsz)
        {
            int pageNo = ParseIntWithDefault(pg, _defaultPageNo, _minimumPageNo);
            int pageSize = ParseIntWithDefault(pgsz, _defaultPageSize, _minimumPageSize);
            int skipCount = pageSize * (pageNo - 1);

            return new PageAPIOptions(pageNo, pageSize, skipCount);
        }

        public HttpResponseMessage Get(string q, string g = "", string pg = "", string pgsz = "")
        {
            PageAPIOptions pageOptions = ParsePageAPIOptions(pg, pgsz);

            int genreId = ParseIntWithDefault(g);

            HttpResponseMessage responseMessage;
            using (var context = new ChinookEntities())
            {
                int totalCount = context.Tracks
                    .Where(t => t.Name.Contains(q) && (t.Genre.GenreId.Equals(genreId) || genreId.Equals(0)))
                    .Count();

                List<TrackDTO> tracks = new List<TrackDTO>();
                tracks.AddRange(
                    context.Tracks
                    .Where(t => t.Name.Contains(q) && (t.Genre.GenreId.Equals(genreId) || genreId.Equals(0)))
                    .OrderBy(t => t.Name)
                    .Select(t => new TrackDTO {
                        TrackId = t.TrackId,
                        Name = t.Name,
                        Genre = t.Genre.Name,
                        Milliseconds = t.Milliseconds,
                        UnitPrice = t.UnitPrice
                    })
                    .Skip(pageOptions.SkipCount)
                    .Take(pageOptions.PageSize)
                    .AsEnumerable()
                );

                responseMessage = Request.CreateResponse<SearchResult>(
                    HttpStatusCode.OK,
                    new SearchResult() {
                        totalCount = totalCount,
                        pageNo = pageOptions.PageNo,
                        pageSize = pageOptions.PageSize,
                        tracks = tracks
                    });
            }

            return responseMessage;
        }

    }

    public class SearchResult
    {
        public int totalCount { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public List<TrackDTO> tracks { get; set; }
    }
}