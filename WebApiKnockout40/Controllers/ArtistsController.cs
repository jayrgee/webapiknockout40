﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiKnockout40.Model;
using WebApiKnockout40.Model.DTO;

namespace WebApiKnockout40.Controllers
{
    public class ArtistsController : ApiController
    {
        public HttpResponseMessage Get(string name)
        {
            HttpResponseMessage responseMessage;
            using (var context = new ChinookEntities())
            {
                //var artists = context.Artists.Where(a => a.Name.Contains(name)).AsEnumerable();

                // starts with, order by name  
                var artists = context.Artists
                    .Where(a => a.Name.StartsWith(name))
                    .OrderBy(a => a.Name)
                    .AsEnumerable();

                var artistsDTO = new List<ArtistDTO>();
                if (artists != null)
                {
                    foreach (var artist in artists)
                    {
                        artistsDTO.Add(new ArtistDTO { ArtistId = artist.ArtistId, Name = artist.Name });
                    }
                    responseMessage = Request.CreateResponse<List<ArtistDTO>>(HttpStatusCode.OK, artistsDTO);
                }
                else
                {
                    responseMessage = Request.CreateErrorResponse(HttpStatusCode.NotFound, new HttpError("Artist not found!"));
                }


            }
            return responseMessage;
        }
    }
}