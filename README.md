#WebApiKnockout4.0#

A .NET 4.0 port of the demo application from [Knockout.js with Web API : The perfect compination](https://chsakell.com/2013/10/19/knockout-js-with-web-api-the-perfect-compination/) (sic).

The [original source code](https://github.com/chsakell/webapiknockout) (targets .NET 4.5)

[This version](https://bitbucket.org/jayrgee/webapiknockout40) of the app also includes:

* A demonstration of [Select2](https://select2.github.io/) with AJAX

* A demonstration of [Select2](https://select2.github.io/) with AJAX and [Bootstrap](http://getbootstrap.com/)